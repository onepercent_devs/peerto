const expect = require('expect.js')
const engine = require('../engine')
const moment = require('moment')

describe('the loans engine', function() {

    let loan, taxes

    beforeEach(() => {
        loan = {
            principal: 2000.00,
            monthlyRate: 0.0194,
            instalments: 3,
            startDate: moment().format('YYYY-MM-DD'),
            firstPaymentOffsetInDays: 0
        }
        taxes = {
            oneTimeTax: 0.0038,
            monthlyTax: 0.000041
        }
    })

    describe.only('when calculating payments for a loan', function() {

        it('should', function() {
            const calculations = engine.calculateMonthlyPayments(loan)(taxes)
            console.log(JSON.stringify(calculations, null, 2))
            expect(calculations.originalDate).to.be(loan.startDate)
            expect(calculations.dailyRate).to.be('0.0006406790')
            expect(calculations.annualRate).to.be('0.2593184060')
        })

    })

})