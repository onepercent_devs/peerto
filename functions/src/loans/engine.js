const moment = require('moment')
const Decimal = require('decimal.js')
const _ = require('lodash')
const ROUNDED_DAYS_IN_MONTH = 30
const DAILY_RATE_INDEX = 1/30
const ANNUAL_RATE_INDEX = 12
Decimal.set({ precision: 10 })

const calculateMonthlyPayments = loan => taxes => {

    const context = _initContext(loan)(taxes)

    const payments = _compose(
        formatOutput,
        taxCalculation,
        compensatoryInterests,
        monthlyPaymentCalculation,
        calculateAccumulatedPriceFactor,
        nominalAndPriceRateFactors,
        paymentDates
    )(context)(new Array(context.instalments))

    return {
        originalPrincipal: _roundToCurrency(context.originalPrincipal),
        principalWithTax: _roundToCurrency(context.principalWithTax),
        originalDate: _formatDate(context.originalDate),
        dailyRate: _roundToRate(context.dailyRate),
        annualRate: _roundToRate(context.annualRate),
        accumulatedPriceFactor: _roundToRate(context.accumulatedPriceFactor),
        payments
    }
}

const paymentDates = ({ originalDate, dueDateReference, roundedDaysFromLastMonth, roundedDaysTillDueDate }) => payments => _.map(payments, payment => {
    const previousDueDate = dueDateReference.clone()
    const dueDate = _addOneMonth(dueDateReference)
    const endOfLastMonth = previousDueDate.clone().endOf('month')
    return {
        previousDueDate,
        dueDate,
        endOfLastMonth,
        roundedDaysFromStart: _differenceInDays(originalDate)(dueDate)(true),
        calendarDaysFromStart: _differenceInDays(originalDate)(dueDate)(),
        roundedDaysFromLastMonth,
        roundedDaysTillDueDate
    }
})

const nominalAndPriceRateFactors = context => {
    let nominalRateFactor
    return payments => _.map(payments, payment => {
        if (!nominalRateFactor) {
            nominalRateFactor = context.decimalMonthlyRate
        } else {
            nominalRateFactor = context.decimalMonthlyRate
                .plus(1)
                .times(nominalRateFactor)
                .plus(context.decimalMonthlyRate)
        }
        const priceFactor = new Decimal(1).dividedBy(nominalRateFactor.plus(1))
        return { ...payment, nominalRateFactor, priceFactor }
    })
}

const calculateAccumulatedPriceFactor = context => payments => {
    context.accumulatedPriceFactor = _.reduce(payments, (memo, { priceFactor }) => priceFactor.plus(memo), 0)
    return payments
}

const monthlyPaymentCalculation = ({ principalWithTax, accumulatedPriceFactor }) => payments => _.map(payments, payment => ({ ...payment, monthlyPayment: principalWithTax.dividedBy(accumulatedPriceFactor) }) )

const compensatoryInterests = ({ principalWithTax, dailyRate, roundedDaysFromLastMonth, roundedDaysTillDueDate }) => {
    let outstandingBalance = new Decimal(principalWithTax)
    return payments => payments.map( payment => {
        const { monthlyPayment } = payment
        const lastMonthInterest = _applyRate(outstandingBalance)(dailyRate)(roundedDaysFromLastMonth)
        const currentMonthInterest = _applyRate(outstandingBalance.plus(lastMonthInterest))(dailyRate)(roundedDaysTillDueDate)
        const monthlyPrincipal = monthlyPayment
            .minus(lastMonthInterest)
            .minus(currentMonthInterest)
        outstandingBalance = outstandingBalance
            .plus(lastMonthInterest)
            .plus(currentMonthInterest)
            .minus(monthlyPayment)
        return {
            ...payment,
            lastMonthInterest,
            currentMonthInterest,
            monthlyPrincipal,
            predictedOutstandingBalance: outstandingBalance
        }
    })
}

const taxCalculation = ({ decimalMonthlyTax }) => payments => _.map(payments, payment => {
    return {
        ...payment,
        financialOperationTax: payment.monthlyPrincipal.times(decimalMonthlyTax).times(payment.calendarDaysFromStart)
    }
})

const formatOutput = context => payments => _.map(payments, payment =>{
     return {
         ...payment,
         dueDate: _formatDate(payment.dueDate),
         endOfLastMonth: _formatDate(payment.endOfLastMonth),
         previousDueDate: _formatDate(payment.previousDueDate),
         nominalRateFactor: _roundToRate(payment.nominalRateFactor),
         priceFactor: _roundToRate(payment.priceFactor),
         monthlyPayment: _roundToCurrency(payment.monthlyPayment),
         monthlyPrincipal: _roundToCurrency(payment.monthlyPrincipal),
         lastMonthInterest: _roundToCurrency(payment.lastMonthInterest),
         currentMonthInterest: _roundToCurrency(payment.currentMonthInterest),
         financialOperationTax: _roundToCurrency(payment.financialOperationTax),
         predictedOutstandingBalance: _roundToCurrency(payment.predictedOutstandingBalance)
     }
})

const _initContext = ({ principal, monthlyRate, instalments, startDate, firstPaymentOffsetInDays = 0 }) => ({ oneTimeTax = 0, monthlyTax }) => {

    const originalDate = moment(startDate, 'YYYY-MM-DD', true)
    const dueDateReference = originalDate.clone().add(firstPaymentOffsetInDays, 'days')
    // initializations
    const originalPrincipal = new Decimal(principal)
    const loanStartDayOfMonth = dueDateReference.format('D')
    const roundedDaysTillDueDate = loanStartDayOfMonth > ROUNDED_DAYS_IN_MONTH ? ROUNDED_DAYS_IN_MONTH : loanStartDayOfMonth
    const roundedDaysFromLastMonth = ROUNDED_DAYS_IN_MONTH - roundedDaysTillDueDate
    return {
        originalPrincipal,
        principalWithTax: originalPrincipal.dividedBy(new Decimal(1).minus(oneTimeTax)),
        originalDate,
        dueDateReference,
        decimalMonthlyRate: new Decimal(monthlyRate),
        decimalMonthlyTax: new Decimal(monthlyTax),
        loanStartDayOfMonth,
        roundedDaysTillDueDate,
        roundedDaysFromLastMonth,
        dailyRate: _equivalentRate(monthlyRate)(DAILY_RATE_INDEX),
        annualRate: _equivalentRate(monthlyRate)(ANNUAL_RATE_INDEX),
        principal,
        instalments,
        oneTimeTax,
        accumulatedPriceFactor: new Decimal(0)
    }
}

const _compose = (...fns) => context => collection => fns.reduceRight((memo, fn) => fn(context)(memo), collection)
const _addOneMonth = date => date.add(1, 'month').clone()
const _differenceInDays = start => date => round => round ? date.diff(start, 'days') * ROUNDED_DAYS_IN_MONTH : date.diff(start, 'days')
const _equivalentRate = rate => index => new Decimal(rate).add(1).toPower(index).minus(1)
const _applyRate = value => rate => index => value.times(rate.add(1).toPower(index).minus(1))
const _roundToCurrency = value => value.toFixed(2)
const _roundToRate = value => value.toFixed(10)
const _formatDate = date => date.format('YYYY-MM-DD')

module.exports = {
    calculateMonthlyPayments
}