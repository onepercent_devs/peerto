const getConfig = require('../components/getConfig')

module.exports = {
    expirationDays: getConfig('cielo.expirationDays') || 3,
    tz: getConfig('app.timezone') || 'America/Sao_Paulo'
}
