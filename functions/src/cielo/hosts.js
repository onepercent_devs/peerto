const getConfig = require('../components/getConfig')

module.exports = {
    apiUrl: (getConfig('cielo.apiUrl') || 'https://apisandbox.cieloecommerce.cielo.com.br') + '/1/sales',
    queryUrl: (getConfig('cielo.queryUrl') || 'https://apiquerysandbox.cieloecommerce.cielo.com.br') + '/1/sales'
}