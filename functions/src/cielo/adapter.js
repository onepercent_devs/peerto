const axios = require('axios')
const timezone = require('moment-timezone')
const { MerchantId, MerchantKey } = require('./credentials.js')
const { apiUrl, queryUrl } = require('./hosts.js')
const { expirationDays, tz } = require('./configs.js')

const adapter = {

    billet: async ({ id, amount, user }) => {
        const { document, token: { name }, address: { street, number, zipCode, neighborhood, city, state, complement }} = user
        const headers = {
            headers: {
                MerchantId,
                MerchantKey
            }
        }
        const request = {
            MerchantOrderId: id,
            Customer: {
                Name: name,
                Identity: document,
                Address: {
                    Street: street,
                    Number: number,
                    Complement: complement,
                    ZipCode : zipCode,
                    District: neighborhood,
                    City: city,
                    State : state,
                    Country: 'BRA'
                }
            },
            Payment: {
                Type:'Boleto',
                Amount: amount,
                Provider: 'bradesco2',
                Address: `${street}, ${number}/${complement} - ${city}/${state}`,
                BoletoNumber: timezone().format('x'),
                Assignor: 'goP2P Lending Platform',
                Demonstrative: 'Teste demonstrativo',
                ExpirationDate: timezone.tz(tz).add(expirationDays, 'days').format('YYYY-MM-DD'),
                Identification: document,
                Instructions: 'Não aceitar após a data de vencimento.'
            }
        }
        const { data } = await axios.post(apiUrl, request, headers)
        return data
    },
    
    checkPayment: async (paymentId) => {
        const headers = {
            headers: {
                MerchantId,
                MerchantKey
            }
        }
        const { data } = await axios.get(`${queryUrl}/${paymentId}`, headers)
        return adapter.parseStatus(data)
    },

    parseStatus: (order) => {
        const status = {
            '0': 'NotFinished', // ALL Aguardando atualização de status
            '1': 'Authorized', // ALL Pagamento apto a ser capturado ou definido como pago
            '2': 'PaymentConfirmed', // ALL Pagamento confirmado e finalizado
            '3': 'Denied', // CC + CD + TF Pagamento negado por Autorizador
            '10': 'Voided', // ALL Pagamento cancelado
            '11': 'Refunded', // CC + CD Pagamento cancelado após 23:59 do dia de autorização
            '12': 'Pending', // ALL Aguardando Status de instituição financeira
            '13': 'Aborted', // ALL Pagamento cancelado por falha no processamento ou por ação do AF
            '20': 'Scheduled', // CC Recorrência agendada
        }
        order.Payment.originalStatus = order.Payment.Status
        order.Payment.status = status[order.Payment.Status]
        return order
    }

}

module.exports = adapter