const functions = require("firebase-functions")
const get = require('lodash/get')

module.exports = {
    pay: get(functions.config(), 'pay.url') || 'http://127.0.0.1:1337',
    transalvadorApp: get(functions.config(), 'transalvador.url') || 'http://wsnoedesenv.salvador.ba.gov.br/wszausuarioapp/ws_usuarioApp.asmx?wsdl',
    transalvadorRules: get(functions.config(), 'transalvador.url') || 'http://wsnoedesenv.salvador.ba.gov.br/wszadados/ws_dadosZonaAzul.asmx?wsdl',
    s2auth: 'https://us-central1-auth-b1c01.cloudfunctions.net'
}