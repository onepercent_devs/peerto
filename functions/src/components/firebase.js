const admin = require('firebase-admin')
let initialized

module.exports = {
    check: function() {
        if (!initialized) {
            admin.initializeApp()
            admin.firestore().settings({ timestampsInSnapshots: true })
            initialized = true
        }
    },
    firestore: function() {
        this.check()
        return admin.firestore()
    },
    auth: function() {
        this.check()
        return admin.auth()
    }
}