const functions = require('firebase-functions')

class ALREADY_CREATED {
    constructor(message, info) {
        this.name = 'ALREADY_CREATED'
        this.message = message || 'Resource already created'
        this.info = info
    }
}

class NOT_FOUND {
    constructor(message, info) {
        this.name = 'NOT_FOUND'
        this.message = message || 'Resource not found'
        this.info = info
    }
}

class UNAUTHENTICATED {
    constructor(message, info) {
        this.name = 'UNAUTHENTICATED'
        this.message = message || 'Invalid credentials'
        this.info = info
    }
}

class UNAUTHORIZED {
    constructor(message, info) {
        this.name = 'UNAUTHORIZED'
        this.message = message || 'You lack rights to perform this action'
        this.info = info
    }
}

class VALIDATION_ERROR {
    constructor(message, info) {
        this.name = 'VALIDATION_ERROR'
        this.message = message
        this.info = info
    }
}

class PAYMENT_REJECTED {
    constructor(message, info) {
        this.name = 'PAYMENT_REJECTED'
        this.message = message
        this.info = info
    }
}

class LOCKED_RESOURCE {
    constructor(message, info) {
        this.name = 'LOCKED_RESOURCE'
        this.message = message
        this.info = info
    }
}

const toHTTP = (error, res) => {

    const { message } = error

    let status = 500
    if (error instanceof UNAUTHORIZED) status = 401
    if (error instanceof PAYMENT_REJECTED) status = 402
    if (error instanceof NOT_FOUND) status = 404
    if (error instanceof ALREADY_CREATED) status = 409
    if (error instanceof VALIDATION_ERROR) status = 422
    if (error instanceof LOCKED_RESOURCE) status = 423

    return res.status(status).json({ message })
}

const toFirebaseError = (error) => {

    const { message, action } = error

    let code = 'internal'
    if (error instanceof UNAUTHENTICATED) code = 'unauthenticated'
    if (error instanceof UNAUTHORIZED) code = 'failed-precondition'
    if (error instanceof PAYMENT_REJECTED) code = 'failed-precondition'
    if (error instanceof NOT_FOUND) code = 'not-found'
    if (error instanceof ALREADY_CREATED) code = 'already-exists'
    if (error instanceof VALIDATION_ERROR) code = 'invalid-argument'
    if (error instanceof LOCKED_RESOURCE) code = 'failed-precondition'

    throw new functions.https.HttpsError(code, message, action)
}

module.exports = { 
    ALREADY_CREATED,
    NOT_FOUND,
    UNAUTHORIZED,
    UNAUTHENTICATED,
    VALIDATION_ERROR,
    PAYMENT_REJECTED,
    LOCKED_RESOURCE,
    toHTTP,
    toFirebaseError
}
