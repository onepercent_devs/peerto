const _ = require('lodash')
const { UNAUTHORIZED } = require('./errors')
const ROLES = {
        INVESTOR: 'investor',
        TAKER: 'taker',
        ADMIN: 'admin'
    }
const acl = {
    recharges: {
        create: [ ROLES.INVESTOR ],
        process: [ ROLES.INVESTOR ]
    }
}

module.exports = {
    check: (resource, action, role) => {
        if (!_.includes(acl[resource][action], role)) throw new UNAUTHORIZED(`Unauthorized access to resource ${resource}/${action} by ${role}`)
    }
}
