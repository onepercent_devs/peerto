const functions = require('firebase-functions')
const get = require('lodash/get')

module.exports = (path) => {
    return get(functions.config(), path)
}