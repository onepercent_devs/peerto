const expect = require('expect.js')
const errors = require('../errors')
const functions = require('firebase-functions')

describe('The errors component', () => {

    let res, expectedStatus, expectedError, expectedCode

    beforeEach(() => {
        res = {
            status: function(status) {
                expect(status).to.be(expectedStatus)
                return this
            },
            json: function(error) {
                expect(error).to.eql(expectedError)
            }
        }
    })

    describe('when converting an error to http', () => {

        it('should convert an UNAUTHORIZED error to a 401 HTTP error', () => {
            expectedStatus = 401
            expectedError = { message: 'You lack rights to perform this action' }
            errors.toHTTP(new errors.UNAUTHORIZED(), res)
        })

        it('should convert a PAYMENT_REJECTED error to a 402 HTTP error', () => {
            expectedStatus = 402
            expectedError = { message: 'pay me' }
            errors.toHTTP(new errors.PAYMENT_REJECTED('pay me'), res)
        })

        it('should convert a NOT_FOUND error to a 404 HTTP error', () => {
            expectedStatus = 404
            expectedError = { message: 'Resource not found' }
            errors.toHTTP(new errors.NOT_FOUND(), res)
        })

        it('should convert an ALREADY_CREATED error to a 409 HTTP error', () => {
            expectedStatus = 409
            expectedError = { message: 'Resource already created' }
            errors.toHTTP(new errors.ALREADY_CREATED(), res)
        })

        it('should convert a VALIDATION_ERROR error to a 422 HTTP error', () => {
            expectedStatus = 422
            expectedError = { message: 'invalid something' }
            errors.toHTTP(new errors.VALIDATION_ERROR('invalid something'), res)
        })

        it('should convert a LOCKED_RESOURCE error to a 429 HTTP error', () => {
            expectedStatus = 423
            expectedError = { message: 'nope' }
            errors.toHTTP(new errors.LOCKED_RESOURCE('nope'), res)
        })

    })

    describe('when converting an error to firebase error code', () => {

        const testIt = (error) => {
            try {
                errors.toFirebaseError(error)
                expect.fail('should have failed')
            } catch (error) {
                expect(error.code).to.be(expectedCode)
                expect(error).to.be.a(functions.https.HttpsError)
                expect(error.message).to.be(expectedError)
            }
        }

        it('should throw an unauthenticated firebase error for an UNAUTHENTICATED error', () => {
            expectedCode = 'unauthenticated'
            expectedError = 'Invalid credentials'
            testIt(new errors.UNAUTHENTICATED())
        })

        it('should throw an failed-precondition firebase error for an UNAUTHORIZED error', () => {
            expectedCode = 'failed-precondition'
            expectedError = 'You lack rights to perform this action'
            testIt(new errors.UNAUTHORIZED())
        })

        it('should throw an failed-precondition firebase error for an PAYMENT_REJECTED error', () => {
            expectedCode = 'failed-precondition'
            expectedError = 'pay me'
            testIt(new errors.PAYMENT_REJECTED('pay me'))
        })

        it('should throw an not-found firebase error for an NOT_FOUND error', () => {
            expectedCode = 'not-found'
            expectedError = 'Resource not found'
            testIt(new errors.NOT_FOUND())
        })

        it('should throw an already-exists firebase error for an ALREADY_CREATED error', () => {
            expectedCode = 'already-exists'
            expectedError = 'Resource already created'
            testIt(new errors.ALREADY_CREATED())
        })

        it('should throw an invalid-argument firebase error for an VALIDATION_ERROR error', () => {
            expectedCode = 'invalid-argument'
            expectedError = 'invalid something'
            testIt(new errors.VALIDATION_ERROR('invalid something'))
        })

        it('should throw an failed-precondition firebase error for an LOCKED_RESOURCE error', () => {
            expectedCode = 'failed-precondition'
            expectedError = 'nope'
            testIt(new errors.LOCKED_RESOURCE('nope'))
            
        })

    })

})
