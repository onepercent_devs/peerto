const Ajv = require('ajv')
const errors = require('./errors')
const documentValidator = require('./documentValidator')

module.exports = (schema, data) => {

    const ajv = new Ajv({ removeAdditional: true })
    
    ajv.addKeyword('cpfCnpj', {
        
        validate: function val(sch, document) {
            val.errors = []
            
            const valid = documentValidator.validate(document)

            if (!valid)
                val.errors.push({
                    keyword: "cpfCnpj",
                    message: "attribute should be a valid cpf/cnpj",
                    params: {
                        keyword: "cpfCnpj"
                    }
                })

            return valid
        },
        errors: true
      });

    const valid = ajv.validate(schema, data)

    if (!valid)
        throw new errors.VALIDATION_ERROR(ajv.errorsText())

}
