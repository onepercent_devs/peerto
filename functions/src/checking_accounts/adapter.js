const _ = require('lodash')
const firebase = require('../components/firebase')
const COLLECTION = 'checkingAccount'

const adapter = {

    async balance(userId) {
        const snapshot = await firebase.firestore()
            .collection(COLLECTION)
            .where('userId', '==', userId)
            .orderBy('created', 'desc')
            .limit(1)
            .get()
        if (snapshot.empty) return { creditAmount: 0, debitAmount: 0 }
        return _.pick(snapshot.docs[0].data(), [ 'creditAmount', 'debitAmount' ])
    },
    async register(record) {
        return await firebase.firestore()
            .collection(COLLECTION)
            .add(record)
    }
}

module.exports = adapter
