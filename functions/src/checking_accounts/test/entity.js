const expect = require('expect.js')
const sinon = require('sinon')
const moment = require('moment')
const entity = require('../entity')
const adapter = require('../adapter')
const lockAdapter = require('../../locks/adapter')
const errors = require('../../components/errors')

describe('The checking accounts entity', () => {

    describe('when getting balance', () => {

        it('return whatever the adapter returns', async () => {
            const balanceStub = sinon.stub(adapter, 'balance').resolves(500)
            expect(await entity.balance()).to.be(500)
            balanceStub.restore()
        })

    })

    describe('when making a debit', () => {

        let lockStub, balanceStub, registerStub, unlockStub

        const debit = {
            userId: 'id',
            amount: 1000,
            requester: 'resource',
            resourceId: 'resourceId',
            description: 'description'
        }

        beforeEach(() => {
            balanceStub = sinon.stub(adapter, 'balance')
            registerStub = sinon.stub(adapter, 'register')
            lockStub = sinon.stub(lockAdapter, 'lock')
            unlockStub = sinon.stub(lockAdapter, 'unlock')
        })

        afterEach(() => {
            balanceStub.restore()
            registerStub.restore()
            lockStub.restore()
            unlockStub.restore()
        })

        it('should throw an error and unlock account if debited account has insufficient funds', async () => {
            lockStub.resolves({ id: 'lockId' })
            balanceStub.resolves({ creditAmount: 0, debitAmount: 0 })
            try {
                await entity.debit(debit)
                expect().fail('should have thrown an error')
            } catch (error) {
                expect(error).be.a(errors.PAYMENT_REJECTED)
                expect(error.message).to.be('Insufficient funds')
                expect(unlockStub.calledOnce).to.be.ok()
            }
        })

        it('should call register with the correct debit data', async () => {
            lockStub.resolves({ id: 'lockId' })
            balanceStub.resolves({ creditAmount: 1100, debitAmount: 0 })
            registerStub.resolves({ id: 'debitId' })
            const expectedDebit = {
                userId: 'id',
                created: moment().format(),
                type: 'DEBIT',
                amount: -1000,
                debitAmount: -1000,
                creditAmount: 1100,
                description: 'description',
                requesterResource: 'resource',
                resourceId: 'resourceId'
            }

            await entity.debit(debit)
            expect(registerStub.getCall(0).args[0]).to.eql(expectedDebit)
        })

        it('should call lock, balance and unlock with correct params', async () => {
            lockStub.resolves({ id: 'lockId' })
            balanceStub.resolves({ creditAmount: 1100, debitAmount: 0 })
            registerStub.resolves({ id: 'debitId' })
            await entity.debit(debit)

            expect(balanceStub.getCall(0).args[0]).to.eql('id')
            expect(lockStub.getCall(0).args).to.eql([ 'id', 'checking_account', 'resource' ])
            expect(unlockStub.getCall(0).args).to.eql([ 'lockId' ])
        })

    })

    describe('when making a credit', () => {

        let lockStub, balanceStub, registerStub, unlockStub

        const credit = {
            userId: 'id',
            amount: 1000,
            requester: 'resource',
            resourceId: 'resourceId',
            description: 'description'
        }

        beforeEach(() => {
            balanceStub = sinon.stub(adapter, 'balance')
            registerStub = sinon.stub(adapter, 'register')
            lockStub = sinon.stub(lockAdapter, 'lock')
            unlockStub = sinon.stub(lockAdapter, 'unlock')
        })

        afterEach(() => {
            balanceStub.restore()
            registerStub.restore()
            lockStub.restore()
            unlockStub.restore()
        })

        it('should call register with the correct credit data', async () => {
            lockStub.resolves({ id: 'lockId' })
            registerStub.resolves({ id: 'creditId' })
            balanceStub.resolves({ creditAmount: 1100, debitAmount: 0 })
            const expectedCredit = {
                userId: 'id',
                created: moment().format(),
                type: 'CREDIT',
                amount: 1000,
                debitAmount: 0,
                creditAmount: 2100,
                description: 'description',
                requesterResource: 'resource',
                resourceId: 'resourceId'
            }

            await entity.credit(credit)
            expect(registerStub.getCall(0).args[0]).to.eql(expectedCredit)
        })

        it('should call lock, balance and unlock with correct params', async () => {
            lockStub.resolves({ id: 'lockId' })
            registerStub.resolves({ id: 'creditId' })
            balanceStub.resolves({ creditAmount: 1100, debitAmount: 0 })
            await entity.credit(credit)
            
            expect(balanceStub.getCall(0).args[0]).to.eql('id')
            expect(lockStub.getCall(0).args).to.eql([ 'id', 'checking_account', 'resource' ])
            expect(unlockStub.getCall(0).args).to.eql([ 'lockId' ])
        })

    })

    describe('when rollbacking registers', () => {

        const registry = {
            userId: 'id',
            amount: 1000,
            requester: 'resource',
            resourceId: 'resourceId',
            description: 'description'
        }

        it('should send add rollback to description and send a debit to the credit function', async () => {
            const creditStub = sinon.stub(entity, 'credit')
            await entity.rollbackDebit(registry)
            registry.description =  `${registry.description} ROLLBACK`
            expect(creditStub.getCall(0).args[0]).to.eql(registry)
            creditStub.restore()
        })

        it('should send add rollback to description and send a credit to the debit function', async () => {
            const debitStub = sinon.stub(entity, 'debit')
            await entity.rollbackCredit(registry)
            registry.description =  `${registry.description} ROLLBACK`
            expect(debitStub.getCall(0).args[0]).to.eql(registry)
            debitStub.restore()
        })

    })

})
