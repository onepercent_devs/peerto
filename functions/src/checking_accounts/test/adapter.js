const expect = require('expect.js')
const sinon = require('sinon')
const firebase = require('../../components/firebase')

describe('The checking accounts adapter', () => {

    let adapter, firestoreStub

    beforeEach( () => {
        firestoreStub = sinon.stub(firebase, 'firestore')
        adapter = require('../adapter')
    })

    afterEach( () => {
        firestoreStub.restore()
    })

    describe('when getting balance', () => {

        it('should return an empty balance if snapshot is empty', async () => {
            const collectionStub = sinon.stub()
            const whereStub = sinon.stub()
            const orderByStub = sinon.stub()
            const limitStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({ empty: true })
            limitStub.returns({ get: getStub })
            orderByStub.returns({ limit: limitStub })
            whereStub.returns({ orderBy: orderByStub })
            collectionStub.returns({ where: whereStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.balance('userId')).to.eql({ creditAmount: 0, debitAmount: 0 })
            expect(getStub.firstCall.args).to.eql([])
            expect(limitStub.firstCall.args).to.eql([ 1 ])
            expect(orderByStub.firstCall.args).to.eql([ 'created', 'desc' ])
            expect(whereStub.firstCall.args).to.eql([ 'userId', '==', 'userId' ])
            expect(collectionStub.firstCall.args).to.eql([ 'checkingAccount' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

        it('should return the found balance', async () => {
            const collectionStub = sinon.stub()
            const whereStub = sinon.stub()
            const orderByStub = sinon.stub()
            const limitStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({
                docs: [
                    {
                        data: () => { return { userId: 'userId', creditAmount: 150, debitAmount: -35 } }
                    }
                ]
            })
            limitStub.returns({ get: getStub })
            orderByStub.returns({ limit: limitStub })
            whereStub.returns({ orderBy: orderByStub })
            collectionStub.returns({ where: whereStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.balance('userId')).to.eql({ creditAmount: 150, debitAmount: -35 })
        })

    })

    describe('when registering a record', () => {

        it('should save the data and return whatever firestore responds', async () => {
            const collectionStub = sinon.stub()
            const addStub = sinon.stub()

            addStub.resolves({ some: 'save data' })
            collectionStub.returns({ add: addStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.register({ the: 'record' })).to.eql({ some: 'save data' })
            expect(addStub.firstCall.args).to.eql([ { the: 'record' } ])
            expect(collectionStub.firstCall.args).to.eql([ 'checkingAccount' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

})
