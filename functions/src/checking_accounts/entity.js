const moment = require('moment')
const adapter = require('./adapter')
const errors = require('../components/errors')
const locksAdapter = require('../locks/adapter')

const entity = {

    async balance(userId) {
        return await adapter.balance(userId)
    },
    async debit({ userId, amount, requester, resourceId, description }) {
        // lock account
        const lock = await locksAdapter.lock(userId, 'checking_account', requester)
        // get balance
        const balance = await adapter.balance(userId)
        if (amount > balance.creditAmount + balance.debitAmount) {
            await locksAdapter.unlock(lock.id)
            throw new errors.PAYMENT_REJECTED('Insufficient funds')
        }
        // make debit
        const negativeAmount = amount * -1
        const debit = {
            userId,
            created: moment().format(),
            type: 'DEBIT',
            amount: negativeAmount,
            debitAmount: balance.debitAmount + negativeAmount,
            creditAmount: balance.creditAmount,
            description,
            requesterResource: requester,
            resourceId
        }
        const { id } = await adapter.register(debit)
        // unlock
        await locksAdapter.unlock(lock.id)

        return { id, balance, debit }
    },
    async credit({ userId, amount, requester, resourceId, description }) {
        // lock account
        const lock = await locksAdapter.lock(userId, 'checking_account', requester)
        // get balance
        const balance = await adapter.balance(userId)
        // make credit
        const credit = {
            userId,
            created: moment().format(),
            type: 'CREDIT',
            amount,
            debitAmount: balance.debitAmount,
            creditAmount: balance.creditAmount + amount,
            description,
            requesterResource: requester,
            resourceId
        }

        const { id } = await adapter.register(credit)
        // unlock
        await locksAdapter.unlock(lock.id)

        return { id, balance, credit }
    },
    async rollbackDebit({ userId, amount, requester, resourceId, description }) {
        await this.credit({ userId, amount, requester, resourceId, description: `${description} ROLLBACK` })
    },
    async rollbackCredit({ userId, amount, requester, resourceId, description }) {
        await this.debit({ userId, amount, requester, resourceId, description: `${description} ROLLBACK` })
    }
}

module.exports = entity