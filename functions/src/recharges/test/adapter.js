const expect = require('expect.js')
const sinon = require('sinon')
const firebase = require('../../components/firebase')
const moment = require('moment')

describe('The recharges adapter', () => {
    
    let adapter, firestoreStub, momentStub
    
    beforeEach( () => {
        firestoreStub = sinon.stub(firebase, 'firestore')
        momentStub = sinon.stub(moment.prototype, 'format')
        adapter = require('../adapter')
    })

    afterEach( () => {
        firestoreStub.restore()
        momentStub.restore()
    })

    describe('when getting by id', () => {

        it('should return undefined if no doc was found', async () => {
            const collectionStub = sinon.stub()
            const docStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({ exists: false })
            docStub.returns({ get: getStub })
            collectionStub.returns({ doc: docStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.getById('rechargeId')).not.to.be.ok()
            expect(getStub.firstCall.args).to.eql([])
            expect(docStub.firstCall.args).to.eql([ 'rechargeId' ])
            expect(collectionStub.firstCall.args).to.eql([ 'recharges' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

        it('should return the found doc', async () => {
            const collectionStub = sinon.stub()
            const docStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({
                exists: true,
                id: 'id',
                data: () => {
                    return { the: 'data' }
                }
            })
            docStub.returns({ get: getStub })
            collectionStub.returns({ doc: docStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.getById('rechargeId')).to.eql({ id: 'id', the: 'data' })
            expect(getStub.firstCall.args).to.eql([])
            expect(docStub.firstCall.args).to.eql([ 'rechargeId' ])
            expect(collectionStub.firstCall.args).to.eql([ 'recharges' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

    describe('when getting pending recharges', () => {

        it('should return undefined if no doc was found', async () => {
            momentStub.returns('7 days ago')
            const collectionStub = sinon.stub()
            const whereStub = sinon.stub()
            const orderByStub = sinon.stub()
            const limitStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({ empty: true })
            limitStub.returns({ get: getStub })
            orderByStub.returns({ limit: limitStub })
            whereStub.onSecondCall().returns({ orderBy: orderByStub })
            whereStub.onFirstCall().returns({ where: whereStub })
            collectionStub.returns({ where: whereStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.getPending()).not.to.be.ok()
            expect(getStub.firstCall.args).to.eql([])
            expect(limitStub.firstCall.args).to.eql([ 10 ])
            expect(orderByStub.firstCall.args).to.eql([ 'created' ])
            expect(whereStub.firstCall.args).to.eql([ 'status', '==', 'AWAITING_PAYMENT' ])
            expect(whereStub.secondCall.args).to.eql([ 'created', '>=', '7 days ago' ])
            expect(collectionStub.firstCall.args).to.eql([ 'recharges' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

        it('should return undefined if no doc was found', async () => {
            momentStub.returns('7 days ago')
            const collectionStub = sinon.stub()
            const whereStub = sinon.stub()
            const orderByStub = sinon.stub()
            const limitStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({
                empty: false,
                docs: [
                    {
                        id: 'id1',
                        data: () => {
                            return { the: 'data1' }
                        }
                    },
                    {
                        id: 'id2',
                        data: () => {
                            return { the: 'data2' }
                        }
                    }
                ]
            })
            limitStub.returns({ get: getStub })
            orderByStub.returns({ limit: limitStub })
            whereStub.onSecondCall().returns({ orderBy: orderByStub })
            whereStub.onFirstCall().returns({ where: whereStub })
            collectionStub.returns({ where: whereStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.getPending()).to.eql([
                {
                    id: 'id1',
                    the: 'data1'
                },
                {
                    id: 'id2',
                    the: 'data2'
                }
            ])
            expect(getStub.firstCall.args).to.eql([])
            expect(limitStub.firstCall.args).to.eql([ 10 ])
            expect(orderByStub.firstCall.args).to.eql([ 'created' ])
            expect(whereStub.firstCall.args).to.eql([ 'status', '==', 'AWAITING_PAYMENT' ])
            expect(whereStub.secondCall.args).to.eql([ 'created', '>=', '7 days ago' ])
            expect(collectionStub.firstCall.args).to.eql([ 'recharges' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

    describe('when creating a recharge', () => {

        it('should save the data and return whatever firestore responds', async () => {
            momentStub.returns('right now')
            const collectionStub = sinon.stub()
            const addStub = sinon.stub()

            addStub.resolves({ some: 'save data' })
            collectionStub.returns({ add: addStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.create({ the: 'recharge' })).to.eql({ some: 'save data' })
            expect(addStub.firstCall.args).to.eql([{
                the: 'recharge',
                status: 'CREATED',
                created: 'right now'
            }])
            expect(collectionStub.firstCall.args).to.eql([ 'recharges' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

    describe('when updating a recharge', () => {

        it('should save the data and return whatever firestore responds', async () => {
            momentStub.returns('right now')
            const collectionStub = sinon.stub()
            const docStub = sinon.stub()
            const updateStub = sinon.stub()

            updateStub.resolves({ some: 'save data' })
            docStub.returns({ update: updateStub })
            collectionStub.returns({ doc: docStub })
            firestoreStub.returns({ collection: collectionStub })

            await adapter.update('rechargeId', { the: 'recharge' })
            expect(docStub.firstCall.args).to.eql([ 'rechargeId' ])
            expect(updateStub.firstCall.args).to.eql([{
                the: 'recharge',
                updated: 'right now'
            }])
            expect(collectionStub.firstCall.args).to.eql([ 'recharges' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

})
