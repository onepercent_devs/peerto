const adapter = require('./adapter')
const cieloAdapter = require('../cielo/adapter')

const entity = {
    create: adapter.create,
    update: adapter.update,
    getPending: adapter.getPending,
    billet: cieloAdapter.billet,
    checkPayment: cieloAdapter.checkPayment
}

module.exports = entity