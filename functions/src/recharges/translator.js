'use strict'
const functions = require('firebase-functions')
const interactor = require('./interactor')
const validator = require('../components/validator')
const authorizer = require('../components/authorizer')
const { UNAUTHENTICATED, toFirebaseError } = require('../components/errors')
const RESOURCE = 'recharges'

/**
 * Recharges are created and a billet is issued. The creation request returns the recharge id
 * and the billet url.
 * Processing takes the older 10 pending recharges created in the last 7 days and checks if there is
 * any status change on cielo. Is the payment was made, a credit is made on user's checking account.
 * The recharge may be in one of these status: CREATED, AWAITING_PAYMENT, CANCELED OR PAID.
 */

const translator = {
    create: async function({ amount }, { auth }) {
        const schema = {
            type: 'object',
            properties: {
                amount: {
                    type: 'integer'
                }
            },
            required: [ 'amount' ]
        }
        try {
            if (!auth) throw new UNAUTHENTICATED()
            authorizer.check(RESOURCE, 'create', auth.token.role)
            validator(schema, { amount })
            return await interactor.create({ amount, auth })
        } catch (e) {
            console.log(e)
            toFirebaseError(e)
        }
    },
    batchProcess: async () => {
        // TODO: log sync so we can alert if tasker isn't working
        console.log('Started recharges batch processing')
        return await interactor.batchProcess()
    }
}

module.exports = {
    create: functions.https.onCall(translator.create),
    process: functions.https.onCall(translator.batchProcess)
}
