const moment = require('moment')
const _ = require('lodash')
const firebase = require('../components/firebase')
const COLLECTION = 'recharges'

const adapter = {

    getById: async (id) => {
        const snapshot = await firebase.firestore()
            .collection(COLLECTION)
            .doc(id)
            .get()
        if (!snapshot.exists) return
        return { ...snapshot.data(), id: snapshot.id }
    },

    getPending: async () => {
        const snapshot = await firebase.firestore()
            .collection(COLLECTION)
            .where('status', '==', 'AWAITING_PAYMENT')
            .where('created', '>=', moment().subtract(7, 'days').format())
            .orderBy('created')
            .limit(10)
            .get()
        if (snapshot.empty) return
        return snapshot.docs.map( doc => { return  {...doc.data(), id: doc.id } })
    },

    create: async (recharge) => {
        return await firebase.firestore()
            .collection(COLLECTION)
            .add({ ...recharge, status: 'CREATED', created: moment().format() })
    },

    update: async (id, data) => {
        await firebase.firestore()
            .collection(COLLECTION)
            .doc(`${id}`)
            .update({ ...data, updated: moment().format() })
    }

}

module.exports = adapter