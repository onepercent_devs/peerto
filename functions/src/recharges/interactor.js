const entity = require('./entity')
const usersEntity = require('../users/entity')
const checkingAccountsEntity = require('../checking_accounts/entity')

const interactor = {
    create: async ({ amount, auth }) => {
        const user = await usersEntity.getById(auth.uid)
        const { id } = await entity.create({ amount, userId: user.id })
        const billetResponse = await entity.billet({ id, amount, user: { ...auth, ...user } })
        await entity.update(id, { billetResponse, status: 'AWAITING_PAYMENT' })
        return { id, url: billetResponse.Payment.Url }
    },
    batchProcess: async () => {
        const pending = await entity.getPending()
        if (!pending) {
            return console.log('No recharges found, skipping')
        }
        console.log(`Found ${pending.length} pending recharges`)
        await Promise.all(
            pending.map(interactor._process)
        )
    },
    _process: async (recharge) => {
        const order = await entity.checkPayment(recharge.billetResponse.Payment.PaymentId)
        const rechargeId = order.MerchantOrderId
        const payment = order.Payment
        switch (payment.status) {
            case 'PaymentConfirmed':
                console.log(`Hey, got a payment confirmation for recharge '${rechargeId}'`)
                const { id } = await checkingAccountsEntity.credit({
                    userId: recharge.userId,
                    amount: payment.Amount,
                    requester: 'recharges',
                    resourceId: rechargeId,
                    description: 'RECHARGE CREDIT'
                })
                await entity.update(rechargeId, {
                    paymentResponse: order,
                    status: 'PAID',
                    creditId: id
                })
                break;
            case 'Voided', 'Aborted':
                console.log(`Payment regarding recharge '${rechargeId}' was '${payment.status}'`)
                await entity.update(rechargeId, { paymentResponse: order, status: 'CANCELED' })
                break
            default:
                console.log(`Nothing to do with '${rechargeId}', status "${payment.status}"`)
                break;
        }
    }
}

module.exports = interactor