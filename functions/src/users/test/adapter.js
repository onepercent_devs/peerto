const expect = require('expect.js')
const sinon = require('sinon')
const firebase = require('../../components/firebase')

describe('The locks adapter', () => {
    
    let adapter, firestoreStub, sandbox
    
    beforeEach( () => {
        firestoreStub = sinon.stub(firebase, 'firestore')
        adapter = require('../adapter')
    })

    afterEach( () => {
        firestoreStub.restore()
    })

    describe('when getting by id', () => {

        it('should return undefined if no doc was found', async () => {
            const collectionStub = sinon.stub()
            const docStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({ exists: false })
            docStub.returns({ get: getStub })
            collectionStub.returns({ doc: docStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.getById('userId')).not.to.be.ok()
            expect(getStub.firstCall.args).to.eql([])
            expect(docStub.firstCall.args).to.eql([ 'userId' ])
            expect(collectionStub.firstCall.args).to.eql([ 'users' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

        it('should return the found doc', async () => {
            const collectionStub = sinon.stub()
            const docStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({
                exists: true,
                id: 'id',
                data: () => {
                    return { the: 'data' }
                }
            })
            docStub.returns({ get: getStub })
            collectionStub.returns({ doc: docStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.getById('userId')).to.eql({ id: 'id', the: 'data' })
            expect(getStub.firstCall.args).to.eql([])
            expect(docStub.firstCall.args).to.eql([ 'userId' ])
            expect(collectionStub.firstCall.args).to.eql([ 'users' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

})
