const firebase = require('../components/firebase')

const adapter = {
    getById: async (uid) => {
        const snapshot = await firebase.firestore()
            .collection('users')
            .doc(uid)
            .get()
        if (!snapshot.exists) return
        return { ...snapshot.data(), id: snapshot.id }
    }
}

module.exports = adapter