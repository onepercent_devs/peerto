const expect = require('expect.js')
const sinon = require('sinon')
const moment = require('moment')
const firebase = require('../../components/firebase')
const errors = require('../../components/errors')
const tweaks = require('../tweaks')

describe('The locks adapter', () => {
    
    let adapter, firestoreStub
    
    beforeEach( () => {
        firestoreStub = sinon.stub(firebase, 'firestore')
        tweaksStub = sinon.stub(tweaks, 'retries').value(1)
        adapter = require('../adapter')
    })

    afterEach( () => {
        firestoreStub.restore()
    })

    describe('when looking for an active lock', () => {

        it('should return whatever firestore returns', async () => {
            const collectionStub = sinon.stub()
            const whereStub = sinon.stub()
            const getStub = sinon.stub()

            getStub.resolves({ some: 'lock... or not' })
            whereStub.onSecondCall().returns({ get: getStub })
            whereStub.onFirstCall().returns({ where: whereStub })
            collectionStub.returns({ where: whereStub })
            firestoreStub.returns({ collection: collectionStub })

            expect(await adapter.get('userId', 'resource')).to.eql({ some: 'lock... or not' })
            expect(getStub.firstCall.args).to.eql([])
            expect(whereStub.firstCall.args).to.eql([ 'userId', '==', 'userId' ])
            expect(whereStub.secondCall.args).to.eql([ 'resource', '==', 'resource' ])
            expect(collectionStub.firstCall.args).to.eql([ 'locks' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })

    })

    describe('when locking a resource', () => {

        let getStub
        
        beforeEach( () => {
            getStub = sinon.stub(adapter, 'get')
        })

        afterEach( () => {
            getStub.restore()
        })

        it('should throw an error if a lock is found', async () => {
            const logStub = sinon.stub(console, 'log')
            getStub.resolves({ empty: false })

            try {
                await adapter.lock('userId', 'resource', 'requester')
                expect().fail('should have thwron an error')
            } catch (error) {
                expect(error).to.eql(new errors.LOCKED_RESOURCE("Failed to get a lock for resource 'resource' and user 'userId'"))
            }
            logStub.restore()
        })

        it('should create a lock if none was found', async () => {
            momentStub = sinon.stub(moment.prototype, 'format')
            momentStub.returns('right now')
            getStub.resolves({ empty: true })

            const collectionStub = sinon.stub()
            const addStub = sinon.stub()

            addStub.resolves({ some: 'lock data' })
            collectionStub.returns({ add: addStub })
            firestoreStub.returns({ collection: collectionStub })

            await adapter.lock('userId', 'resource', 'requester')
            expect(addStub.firstCall.args).to.eql([{
                userId: 'userId',
                resource: 'resource',
                requester: 'requester',
                created: 'right now'
            }])
            expect(collectionStub.firstCall.args).to.eql([ 'locks' ])
            expect(firestoreStub.calledOnce).to.be.ok()
            momentStub.restore()
        })

    })

    describe('when unlocking a resource', () => {

        it('should send the right params to firestore', async () => {
            const collectionStub = sinon.stub()
            const docStub = sinon.stub()
            const deleteStub = sinon.stub()

            deleteStub.resolves()
            docStub.onFirstCall().returns({ delete: deleteStub })
            collectionStub.returns({ doc: docStub })
            firestoreStub.returns({ collection: collectionStub })

            await adapter.unlock('lockId')
            expect(deleteStub.firstCall.args).to.eql([])
            expect(docStub.firstCall.args).to.eql([ 'lockId' ])
            expect(collectionStub.firstCall.args).to.eql([ 'locks' ])
            expect(firestoreStub.calledOnce).to.be.ok()
        })


    })

})
