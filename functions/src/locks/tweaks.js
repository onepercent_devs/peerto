const getConfig = require('../components/getConfig')

module.exports = {
    retries: getConfig('locks.retries') || 4
}