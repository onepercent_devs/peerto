const retry = require('async-retry')
const firebase = require('../components/firebase')
const errors = require('../components/errors')
const moment = require('moment')
const tweaks = require('./tweaks')
const COLLECTION = 'locks'

const adapter = {
    async get (userId, resource) {
        return await firebase.firestore()
            .collection(COLLECTION)
            .where('userId', '==', userId)
            .where('resource', '==', resource)
            .get()
    },
    async lock (userId, resource, requester) {
        return await retry(async bail => {

            const snapshot = await this.get(userId, resource)

            if (!snapshot.empty) throw new errors.LOCKED_RESOURCE(`Failed to get a lock for resource '${resource}' and user '${userId}'`)

            return await firebase.firestore()
                .collection(COLLECTION)
                .add({ userId, requester, resource, created: moment().format() })
        }, {
            retries: tweaks.retries,
            onRetry: error => {
                console.log(error.message, ', retrying...')
            }
        })

    },
    async unlock (id) {
        await firebase.firestore().collection(COLLECTION).doc(id).delete()
    }
}

module.exports = adapter
