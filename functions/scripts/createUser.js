const admin = require('firebase-admin')

admin.initializeApp({
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "dev-lending-p2p",
        "private_key_id": "48b0d7679f22949b91f2fbfd67c9048ae01007b7",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDlYop57PMNcI2h\nD3zJ00OPeV2D9Z61p8KEy/H3CEJwsFBoqs+1w9bEN2jzdlcU5RTiS+TKTRVYLwjG\n0zk4XoSNo/Wk3Hqr8knYHWWqDD1vOGIrUQ7sE2DMukss27RVcj1McJ5i9iX9QRHX\nkR1UsxGtJL/zyYxL6ci59q35CacTxs9RubbPKTwCkzl7cKCWeu4zOWZRevMFRoad\nwCr8cgL/coLeHA7vO2cAQAjeQOijVMPHvYs8qbSuo+UL7rPfPMtcghewPBYv0UXy\nh+GaoFcRgga8YUbbGFc2xX4UZ5CeF9iXUQDLpJml0Csq3jrBBAaV5JVYn7vMsG0N\nTtzsXvHJAgMBAAECggEAD1OtQwZVGzHJFNjvEqE+I2kNmZKg3yxtb+gcq8/ueI8f\nYE8Bf/is8gFtzs66dNf1tEUz7VyHL5Q89uQIL78JBlnNQQ3GWJTg4ELelyUoObU/\nwfsxjw6gai2MdlvbydXPSLtBqgUedrSRV6EHn7AzXylUdhqieDQPyHU2HKzLcCO5\nLjaGYvv7cs0VP0msjuZphzj4HC28ckB99JGM4lpPoyu0qbY55A7MNB5RQPXPRkGL\nvTSvNQKL3g1sVMXco6K77G9BtuRlYTxquvoCHfj4AyyxezN6JJI/HbBzzF3OR5ZX\nAdWui5zMlXUeqTjZYJ9ryN4otY1NA+UhImpOVlW3rQKBgQD9SU9yATDilmEGXv+L\nD8VztgoCgan4OgzT+uXbxE/b8N+F86W5qKFtRNk0GIoQVIxXA8t0SSeEPN78sEJO\ncmqV621X2moFsR4SSPllhoWo0YHYt1pml7zA+FcxTxD5yI5uy6C1LCPTBqhnPyuk\nJFIBQa0Q7QnFl0bbqP5smK/ImwKBgQDn160N8EPa/AmCEexjEfbfASkdeYmBBUfV\nTMTT7lgrw5t5eLQ1OP+Qrs1ZhUlkfjSE7JwR0xPDjioFwJ++m3N4MbLvh9PNZ0Tj\nX2KTOurNV8DFFq85Hun1EjOEq4WNzuT8lKMAqLg+ZJhLpZLAJnDfIxzxO5a/Pnch\nDobDa7VbawKBgC/5O7fvK9AvBW+MfMBdG1wARcMVxhjC/f0Ej5bhhvK0m9GmRWR1\n4SVuHem1IClZqj8s89Mxq+9rDB02UUjEeRWuQsaaEuzDEFwMQFiPcy93hy2CBPhb\nkx/FqdYoEKIO75NitCy8kIh+wVTU7TIowvOD+gCztN8c+QnP52iwPtMzAoGBAIyV\nX76f/sV/J1TUAX9hmU1IquHQnOy7gv94ebH5SUXgaxqRctUDJxVYqfSqugJR9Q7V\nWx4D3GwsfJbYYwV1xeMQX+yXdHuNL1ygrmmQxGiX2bCpPL9qW3vb+ZltWKLQ3lG6\n38y6gl5zKVZVpFMX/EGSTS6Kb7Kv8hT9dgJyAZrZAoGBAJmxvZjMuYcT8W/FXVjL\ni7WAR6aQyrf3sjj+CrageIDK8cQJMPCxViY9tNCadUAYB4tJudkmYPHFVk1PYnnA\njswPPLmGEstHP05xccebVqe4n/gziQ4/glVQS5sERisFFo8CTztfwyn4WMlBhqDd\n7Lf6P3RPEhQxTzNRbW3odVw/\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-h41zo@dev-lending-p2p.iam.gserviceaccount.com",
        "client_id": "108831073289397545838",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-h41zo%40dev-lending-p2p.iam.gserviceaccount.com"
      }),
    databaseURL: "https://dev-lending-p2p.firebaseio.com"
})

admin.auth().createUser({
    email: "admin@gmail.com",
    emailVerified: false,
    password: "321321321",
    displayName: "André Meirelles",
    disabled: false
}).then( ({ uid }) => {
    return admin.auth().setCustomUserClaims(uid, { role: 'admin' })
}).catch(console.error)