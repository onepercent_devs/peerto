import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import { version } from '../package.json';

window.version = version;

const MOUNT_EL = document.getElementById('root');

if (process.env.NODE_ENV === 'development') {
  const RedBox = require('redbox-react').default
    try {
      ReactDOM.render(<App />, MOUNT_EL)
    } catch (e) {
      ReactDOM.render(<RedBox error={e} />, MOUNT_EL)
    }
  } else {
    ReactDOM.render(<App />, MOUNT_EL)
}

serviceWorker.unregister();
