import React from 'react';
import { Link } from 'react-router-dom';

const NavbarLink = ({ name, path: to, isSelected, isButton, onLinkClicked }) => {
  return (
    <Link
      {...{
        className: `navbar-link
          ${isSelected && "navbar-link--active"}
          ${isButton && "navbar-link--button-like"}`,
        to,
        onClick: () => onLinkClicked(to)
      }}
    >
      { name }
    </Link>
  )
};

export default NavbarLink;
