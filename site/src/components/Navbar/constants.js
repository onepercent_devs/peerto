import { PATHS } from '../../views/App/constants';

export const LINKS = [
  {
    path: PATHS.HOME,
    name: 'Home'
  },
  {
    path: PATHS.PRODUCTS,
    name: 'Produtos'
  },
  {
    path: PATHS.ABOUT_US,
    name: 'Sobre nós'
  },
  {
    path: PATHS.CONTACT,
    name: 'Contato'
  },
  {
    path: PATHS.SIGN_UP,
    name: 'Sign Up',
    isButton: true
  },
]