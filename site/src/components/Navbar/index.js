import React, { Component } from 'react';
import NavbarLink from '../NavbarLink';
import { LINKS } from './constants'

class Navbar extends Component {
  state = { selectedLink: window.location.pathname };

  handlePageChange(selectedLink) {
    this.setState({ selectedLink });
  }

  _renderIcons() {
    const onLinkClicked = selectedLink => this.handlePageChange(selectedLink);
    const { selectedLink } = this.state;

    return LINKS.map((link, index) => {
      return (
        <NavbarLink
          {...{
            key: index,
            className: "navbar__link",
            onLinkClicked,
            isSelected: selectedLink === link.path,
            ...link
          }}
        />
      )
    });
  }

  render() {
    return <div className="navbar">{ this._renderIcons() }</div>;
  }
}

export default Navbar;
