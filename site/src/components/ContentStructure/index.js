import React from 'react';
import { Link } from 'react-router-dom';

const ContentStructure = ({ title, description, link }) => (
  <div className="content-structure">
    <h2 className="content-structure__title">
      { title }
    </h2>
    <p className="content-structure__description">
      { description }
    </p>

    <div className="content-structure__action-link">
      <Link
        {...{
          className: 'button button--uppercase',
          to: link.url
        }}
      >
        { link.description }
      </Link>
    </div>
  </div>
);

export default ContentStructure;