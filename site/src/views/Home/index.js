import React from 'react';
import ContentStructure from '../../components/ContentStructure';
import { MOCK } from './mock';

const Home = () => {
  const [leftColumn, rightColumn] = MOCK;

  return (
    <div className="home">
      <div className="row home__main-content">
        <div className="col-md-5">
          <ContentStructure {...leftColumn}/>
        </div>
        <div className="col-md-5 offset-2">
          <ContentStructure {...rightColumn}/>
        </div>
      </div>
    </div>
  )
};

export default Home;