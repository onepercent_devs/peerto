export const MOCK = [
  {
    title: 'Título usuário que pede empréstimo',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In mattis felis vitae rutrum imperdiet. Pellentesque lacinia nibh in convallis suscipit. Mauris porta tortor tellus, in ullamcorper massa scelerisque eu.',
    link: {
      description: 'Pedir empréstimo',
      url: '#'
    }
  },
  {
    title: 'Título usuário que quer investir',
    description: 'Nullam et efficitur diam, non tempor ante. Morbi vitae dui egestas ipsum vehicula accumsan. Maecenas vestibulum libero id neque dignissim dapibus. Suspendisse commodo purus quis lacus.',
    link: {
      description: 'Investir',
      url: '#'
    }
  },
]