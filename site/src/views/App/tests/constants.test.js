import { PATHS } from '../constants';

describe('APP - Constants', () => {
  it('should have proper paths', () => {
    const expectedPaths = {
      HOME: '/',
      SIGN_UP: '/registre-se'
    };

    expect(PATHS).toEqual(expectedPaths);
  });
})
