export const PATHS = {
  HOME: '/',
  ABOUT_US: '/sobre-nos',
  PRODUCTS: '/produtos',
  CONTACT: '/contato',
  SIGN_UP: '/registre-se',
}