import React from 'react';
import { PATHS } from './constants';

// Views
import Home from '../Home';
import SignUp from '../SignUp';
import NotFound from '../NotFound';

export const ROUTES = [
  {
    path: PATHS.HOME,
    exact: true,
    component: () => <Home/>,
  },
  {
    path: PATHS.SIGN_UP,
    exact: true,
    component: () => <SignUp/>,
  },
  {
    component: () => <NotFound/>,
  },
]