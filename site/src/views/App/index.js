import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from '../../components/Navbar';
import { ROUTES } from './routes';

class App extends Component {

  _renderRoutes = () => {
    return (
      <Switch>
        {ROUTES.map((route, index) => (
          <Route
            {...{
              key: index,
              ...route
            }}
          />
        ))}
      </Switch>
    )
  }

  render() {
    return (
      <Router>
        <div className="app">
          <Navbar/>
          <div className="app__content">
            <div className="container d-flex">
              { this._renderRoutes() }
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
