import firebase from 'firebase'
import 'firebase/firestore'
import config from './config.json'

firebase.initializeApp(config)
firebase.firestore().settings({timestampsInSnapshots: true})

window.firebase = firebase

export default firebase