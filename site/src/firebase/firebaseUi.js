import firebase from 'firebase'

let firebaseUiAuth = null

export function getFirebaseUI() {
    if(!firebaseUiAuth) firebaseUiAuth = new window.firebaseui.auth.AuthUI(firebase.auth())
    return firebaseUiAuth
}